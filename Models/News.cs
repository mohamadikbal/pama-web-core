﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class News
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime PublishDate { get; set; }
        public string ImageBaner { get; set; }
        public bool Status { get; set; }
        public bool ExternalLink { get; set; }
        public int VideoImage { get; set; }
        public string ExternalLinkUrl { get; set; }
        public int Type { get; set; }
        public string UrlVideo { get; set; }
        public string SourceUrl { get; set; }
    }
}
