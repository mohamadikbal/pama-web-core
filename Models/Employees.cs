﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class Employees
    {
        public string EmployeeCode { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Instagram { get; set; }
    }
}
