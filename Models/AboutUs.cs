﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class AboutUs
    {
        public int Id { get; set; }
        public int MainEquipmentNumber { get; set; }
        public int EmployeeNumber { get; set; }
        public int MainOperationNumber { get; set; }
        public int JobsiteNumber { get; set; }
        public int SupportOfficeNumber { get; set; }
        public int AfflicationCompanyNumber { get; set; }
        public int MaintainByNumber { get; set; }
        public bool Status { get; set; }
    }
}
