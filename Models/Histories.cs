﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class Histories
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int No { get; set; }
        public string CompanyName { get; set; }
        public string ProjectName { get; set; }
        public int Year { get; set; }
    }
}
