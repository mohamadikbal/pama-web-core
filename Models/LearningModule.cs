﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class LearningModule
    {
        public int Id { get; set; }
        public string VideoUrl { get; set; }
        public string Description { get; set; }
        public bool Status { get; set; }
        public int LearningModuleType { get; set; }
        public string Pdflink { get; set; }
        public string Name { get; set; }
    }
}
