﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class Kurs
    {
        public int Id { get; set; }
        public string CurrencyCode { get; set; }
        public double ExchangeRate { get; set; }
        public double BuyingValue { get; set; }
    }
}
