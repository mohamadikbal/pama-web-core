﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class Visions
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int No { get; set; }
        public int Language { get; set; }
    }
}
