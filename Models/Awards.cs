﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class Awards
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool Status { get; set; }
        public int AwardCategory { get; set; }
        public int No { get; set; }
        public DateTime AwardDate { get; set; }
    }
}
