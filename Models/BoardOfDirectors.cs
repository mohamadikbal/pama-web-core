﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class BoardOfDirectors
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Possition { get; set; }
        public string Description { get; set; }
        public string ImageUrl { get; set; }
        public bool Status { get; set; }
        public int BodBoc { get; set; }
        public int No { get; set; }
        public int LevelBodboc { get; set; }
        public int Language { get; set; }
    }
}
