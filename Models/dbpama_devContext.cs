﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace PamaWeb.Models
{
    public partial class dbpama_devContext : DbContext
    {
        public dbpama_devContext()
        {
        }

        public dbpama_devContext(DbContextOptions<dbpama_devContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AboutUs> AboutUs { get; set; }
        public virtual DbSet<Awards> Awards { get; set; }
        public virtual DbSet<BoardOfDirectors> BoardOfDirectors { get; set; }
        public virtual DbSet<Employees> Employees { get; set; }
        public virtual DbSet<Histories> Histories { get; set; }
        public virtual DbSet<Kurs> Kurs { get; set; }
        public virtual DbSet<LearningModule> LearningModule { get; set; }
        public virtual DbSet<Missions> Missions { get; set; }
        public virtual DbSet<News> News { get; set; }
        public virtual DbSet<TempNews> TempNews { get; set; }
        public virtual DbSet<Visions> Visions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Data Source=dbpamaweb.crrbv2uepfdy.ap-southeast-1.rds.amazonaws.com,1433;Initial Catalog=dbpama_dev;Trusted_Connection=True;Integrated Security=False;user ID= admin;Password= admin12345");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AboutUs>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AfflicationCompanyNumber).HasColumnName("AfflicationCompanyNUmber");
            });

            modelBuilder.Entity<Awards>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AwardDate).HasDefaultValueSql("('0001-01-01T00:00:00.0000000')");
            });

            modelBuilder.Entity<BoardOfDirectors>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BodBoc).HasColumnName("BOD_BOC");

                entity.Property(e => e.ImageUrl).HasColumnName("imageUrl");

                entity.Property(e => e.LevelBodboc).HasColumnName("LevelBODBOC");
            });

            modelBuilder.Entity<Employees>(entity =>
            {
                entity.HasKey(e => e.EmployeeCode);

                entity.Property(e => e.EmployeeCode).HasColumnName("employeeCode");

                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<Histories>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<Kurs>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<LearningModule>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Pdflink).HasColumnName("PDFLink");
            });

            modelBuilder.Entity<Missions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");
            });

            modelBuilder.Entity<News>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ExternalLinkUrl).HasColumnName("ExternalLinkURL");

                entity.Property(e => e.UrlVideo).HasColumnName("urlVideo");
            });

            modelBuilder.Entity<TempNews>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("tempNews");

                entity.Property(e => e.ExternalLinkUrl).HasColumnName("ExternalLinkURL");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.UrlVideo).HasColumnName("urlVideo");
            });

            modelBuilder.Entity<Visions>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.No).HasColumnName("no");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
