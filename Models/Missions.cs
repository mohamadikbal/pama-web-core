﻿using System;
using System.Collections.Generic;

namespace PamaWeb.Models
{
    public partial class Missions
    {
        public int Id { get; set; }
        public int No { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public int Language { get; set; }
    }
}
