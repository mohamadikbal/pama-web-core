﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using PamaWeb.Models;

namespace PamaWeb.Controllers
{
    public class VisiMisiController : Controller
    {
        private readonly dbpamaContext _context;
        //add for currrent language

        private string _currentLanguage;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }

        public VisiMisiController(dbpamaContext context)
        {
            _context = context;
        }
        public IActionResult Index()
        {
            var culture = CurrentLanguage;
            if (culture == "en")
            {
                ViewBag.Visions = _context.Visions.Where(c => c.Language == 1).ToList();
                ViewBag.Missions = _context.Missions.Where(c => c.Language == 1).ToList();
            }
            else
            {
                ViewBag.Visions = _context.Visions.Where(c => c.Language == 2).ToList();
                ViewBag.Missions = _context.Missions.Where(c => c.Language == 2).ToList();
            }

            return View();
        }
    }
}