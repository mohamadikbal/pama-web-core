﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Localization;
using PamaWeb.Models;

namespace PamaWeb.Controllers
{
    public class NewsController : Controller
    {
        private readonly dbpamaContext _context;

        public NewsController(dbpamaContext context)
        {
            _context = context;
        }
        public IActionResult Index()
         
        {
            ViewBag.News = _context.News.OrderByDescending(c => c.Id).ToList();
            ViewBag.DetailNews = _context.News.ToList();
            ViewBag.Topnews = _context.News.Take(2).ToList();
            ViewBag.Event = _context.News.Where(c=>c.Type==2).ToList();
            ViewBag.Kurs = _context.Kurs.ToList();
            return View();
        }

        public async Task<IActionResult> Details(int? id)
        {

            ViewBag.DetailNews = _context.News.Where(c=>c.Id!=id).Take(3).OrderByDescending(c => c.Id).ToList();

            if (id == null)
            {
                return NotFound();
            }

            var history = await _context.News
                .FirstOrDefaultAsync(m => m.Id == id);
            if (history == null)
            {
                return NotFound();
            }

            return View(history);
        }

        private string _currentLanguage;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        public ActionResult RedirectToDefaultCulture()
        {
            var culture = CurrentLanguage;
            if (culture != "en")
                culture = "en";
            return RedirectToAction("Index", new { culture });
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}