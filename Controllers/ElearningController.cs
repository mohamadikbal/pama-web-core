﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using PamaWeb.Models;


namespace PamaWeb.Controllers
{
    public class ElearningController : Controller
    {
        private readonly dbpamaContext _context;

        public ElearningController(dbpamaContext context)
        {
            _context = context;
        }

        public IActionResult Search(string searchString)
        {

            var learning = from l in _context.LearningModule
                           select l;

            if (!String.IsNullOrEmpty(searchString))
            {
                learning = learning.Where(s => s.Name.Contains(searchString));
            }

            return View(learning.ToList());
        }

        public IActionResult Index(String param, String searchString)
        {

            //default page
            try
            {
                if (param.Equals("1"))
                {
                    //general
                    ViewBag.Video = new List<LearningModule>();
                    ViewBag.Pdf = new List<LearningModule>(); ;
                    ViewBag.General = _context.LearningModule.OrderByDescending(c => c.Id).ToList();
                }
                else if (param.Equals("2"))
                {
                    //video
                    ViewBag.Video = new List<LearningModule>(); ;
                    ViewBag.Pdf = new List<LearningModule>(); ;
                    ViewBag.General = _context.LearningModule.Where(c => c.LearningModuleType == 1).OrderByDescending(c => c.Id).ToList();
                }
                else if (param.Equals("3"))
                {
                    //pdf
                    ViewBag.Video = new List<LearningModule>(); ;
                    ViewBag.Pdf = new List<LearningModule>(); ;
                    ViewBag.General = _context.LearningModule.Where(c => c.LearningModuleType == 2).OrderByDescending(c => c.Id).ToList();

                }
                //else if (searchString)
                //{
                //    var learning = from l in _context.LearningModule
                //                   select l;

                //    if (!String.IsNullOrEmpty(searchString))
                //    {
                //        learning = learning.Where(s => s.Name.Contains(searchString));
                //    }
                //}
            }
            catch
            {
                ViewBag.National = _context.LearningModule.Where(c => c.LearningModuleType == 1).OrderByDescending(c => c.Id).ToList();
                ViewBag.International = _context.LearningModule.Where(c => c.LearningModuleType == 2).OrderByDescending(c => c.Id).ToList();
                ViewBag.General = _context.LearningModule.OrderByDescending(c => c.Id).ToList();
            }

            

            return View();
        }
    }
}