﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PamaWeb.Models;

namespace PamaWeb.Controllers
{
    public class HomeController : Controller
    {
        private readonly dbpamaContext _context;

        private readonly ILogger<HomeController> _logger;

        public HomeController(dbpamaContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            ViewBag.Histories = _context.Histories.OrderByDescending(c => c.Year).ToList();
            ViewBag.News = _context.News.OrderByDescending(c => c.Id).Take(3).ToList();

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
