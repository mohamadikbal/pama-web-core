﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using PamaWeb.Models;

namespace PamaWeb.Controllers
{
    public class AwardController : Controller
    {
        private readonly dbpamaContext _context;

        public AwardController(dbpamaContext context)
        {
            _context = context;
        }
        public IActionResult Index(String param)
        {

            //default page
            try
            {
                if (param.Equals("1"))
                {
                    //all
                    
                    ViewBag.National = new List<Awards>();
                    ViewBag.International = new List<Awards>(); ;
                    ViewBag.General = _context.Awards.OrderByDescending(c => c.Id).ToList();
                }
                else if (param.Equals("2"))
                {
                    //national
                    ViewBag.National = new List<Awards>(); ;
                    ViewBag.International = new List<Awards>(); ;
                    ViewBag.General = _context.Awards.Where(c => c.AwardCategory == 2).OrderByDescending(c => c.Id).ToList();
                }
                else if (param.Equals("3"))
                {
                    //international
                    ViewBag.National = new List<Awards>(); ;
                    ViewBag.International = new List<Awards>(); ;
                    ViewBag.General = _context.Awards.Where(c => c.AwardCategory == 3).OrderByDescending(c => c.Id).ToList();

                }
                else
                {
                    ViewBag.National = _context.Awards.Where(c => c.AwardCategory == 2).Take(2).ToList();
                    ViewBag.International = _context.Awards.Where(c => c.AwardCategory == 3).Take(1).ToList();
                    ViewBag.General = _context.Awards.Where(c => c.AwardCategory != 2 && c.AwardCategory != 3).ToList();
                }
            }
            catch
            {
                //all
                ViewBag.National = new List<Awards>();
                ViewBag.International = new List<Awards>(); ;
                ViewBag.General = _context.Awards.OrderByDescending(c => c.Id).ToList();
            }
            /*
            {
                ViewBag.National = _context.Awards.Where(c => c.AwardCategory == 2).Take(2).ToList();
                ViewBag.International = _context.Awards.Where(c => c.AwardCategory == 3).Take(1).ToList();
                ViewBag.General = _context.Awards.Where(c => c.AwardCategory != 2 && c.AwardCategory != 3).ToList();

            }*/



            return View();
        }

        private string _currentLanguage;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }
        public ActionResult RedirectToDefaultCulture()
        {
            var culture = CurrentLanguage;
            if (culture != "en")
                culture = "en";
            return RedirectToAction("Index", new { culture });
        }


        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}