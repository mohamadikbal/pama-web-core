﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using PamaWeb.Models;
using Microsoft.AspNetCore.Localization;


namespace PamaWeb.Controllers
{
    public class BodBocController : Controller
    {
        private readonly dbpamaContext _context;

        public BodBocController(dbpamaContext context)
        {
            _context = context;
        }

        private string _currentLanguage;
        private string CurrentLanguage
        {
            get
            {
                if (!string.IsNullOrEmpty(_currentLanguage))
                    return _currentLanguage;

                if (string.IsNullOrEmpty(_currentLanguage))
                {
                    var feature = HttpContext.Features.Get<IRequestCultureFeature>();
                    _currentLanguage = feature.RequestCulture.Culture.TwoLetterISOLanguageName.ToLower();
                }

                return _currentLanguage;
            }
        }

        public IActionResult Index()
        {
            var culture = CurrentLanguage;
            if (culture == "en")
            {
                ViewBag.PresidentKomisaris = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 1 && c.Language == 1).ToList();
                ViewBag.Komisaris = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 2 && c.Language == 1).ToList();
                ViewBag.PresidentDirektur = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 3 && c.Language == 1).ToList();
                ViewBag.Direktur = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 4 && c.Language == 1).ToList();

            }
            else
            {
                ViewBag.PresidentKomisaris = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 1 && c.Language == 2).ToList();
                ViewBag.Komisaris = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 2 && c.Language == 2).ToList();
                ViewBag.PresidentDirektur = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 3 && c.Language == 2).ToList();
                ViewBag.Direktur = _context.BoardOfDirectors.Where(c => c.LevelBodboc == 4 && c.Language == 2).ToList();
            }

            return View();
        }
    }
}