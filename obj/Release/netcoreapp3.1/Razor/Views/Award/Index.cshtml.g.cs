#pragma checksum "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "4fee496d9ad32829c74dec81158988ac588ed270"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Award_Index), @"mvc.1.0.view", @"/Views/Award/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\_ViewImports.cshtml"
using PamaWeb;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\_ViewImports.cshtml"
using PamaWeb.Models;

#line default
#line hidden
#nullable disable
#nullable restore
#line 1 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
using Microsoft.AspNetCore.Mvc.Localization;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"4fee496d9ad32829c74dec81158988ac588ed270", @"/Views/Award/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"06e3ab49e30fbb022a2238d7c15e21d728f32179", @"/Views/_ViewImports.cshtml")]
    public class Views_Award_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("rel", new global::Microsoft.AspNetCore.Html.HtmlString("stylesheet"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("href", new global::Microsoft.AspNetCore.Html.HtmlString("~/css/award.css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_2 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("type", new global::Microsoft.AspNetCore.Html.HtmlString("text/css"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_3 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("media", new global::Microsoft.AspNetCore.Html.HtmlString("screen"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_4 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("runat", new global::Microsoft.AspNetCore.Html.HtmlString("server"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("link", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.SelfClosing, "4fee496d9ad32829c74dec81158988ac588ed2705056", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_2);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_3);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_4);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n<div class=\"container\">\r\n    <br />\r\n    <h1 class=\"font-weight-bold text-center mt-5 mb-0 title\">");
#nullable restore
#line 8 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                        Write(Localizer["main_title"]);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</h1>
    <div class=""p-3 container rounded"">
        <div class=""row justify-content-center"">
            <div class=""col text-center dropdown border-0"">
                <button type=""button"" class=""btn btn-outline-warning dropdown-toggle mt-2"" data-toggle=""dropdown"">
                    ");
#nullable restore
#line 13 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
               Write(Localizer["categories"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n                </button>\r\n                <div class=\"dropdown-menu\">\r\n                    <a class=\"dropdown-item\" href=\"Award?param=1\">");
#nullable restore
#line 16 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                             Write(Localizer["dd_province"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                    <a class=\"dropdown-item\" href=\"Award?param=2\">");
#nullable restore
#line 17 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                             Write(Localizer["dd_national"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                    <a class=\"dropdown-item\" href=\"Award?param=3\">");
#nullable restore
#line 18 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                             Write(Localizer["dd_international"]);

#line default
#line hidden
#nullable disable
            WriteLiteral("</a>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row justify-content-center text-center mb-md-5\">\r\n");
#nullable restore
#line 24 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
          
            List<Awards> generaltop = ViewBag.International;
            int i = 0;
            if (generaltop.Count > 0)
            {
                foreach (var general in generaltop)
                {
                    if (i == 0)
                    {
                        String imageurl = PamaWeb.GeneralClass.GeneralClass.backendUrl + "" + general.ImageUrl;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                        <div class=""col-md-5 mb-2"">
                            <figure>
                                <a href=""#"" class=""d-block h-100 mb-4"" onclick=""return false;"">
                                    <img class=""imgmainaward img-fluid img-thumbnail""");
            BeginWriteAttribute("src", " src=\"", 1795, "\"", 1810, 1);
#nullable restore
#line 37 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
WriteAttributeValue("", 1801, imageurl, 1801, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"img_award\">\r\n                                </a>\r\n                                <figcaption class=\"caption-top p-2 rounded text-center bg-warning font-weight-bold mt-n2\">");
#nullable restore
#line 39 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                                                                                     Write(general.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</figcaption>\r\n                            </figure>\r\n                        </div>\r\n");
#nullable restore
#line 42 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                    }
                    i++;
                }
            }
        

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n    <div class=\"row justify-content-around text-center\">\r\n");
#nullable restore
#line 49 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
          
            List<Awards> generalsecond = ViewBag.National;
            if (generalsecond.Count > 0)
            {
                foreach (var general in generalsecond)
                {

                    String imageurl = PamaWeb.GeneralClass.GeneralClass.backendUrl + "" + general.ImageUrl;

#line default
#line hidden
#nullable disable
            WriteLiteral(@"                    <div class=""col-md-5 mb-2"">
                        <figure>
                            <a href=""#"" class=""d-block h-100 mb-4"" onclick=""return false;"">
                                <img class=""imgmainaward img-fluid img-thumbnail""");
            BeginWriteAttribute("src", " src=\"", 2826, "\"", 2841, 1);
#nullable restore
#line 60 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
WriteAttributeValue("", 2832, imageurl, 2832, 9, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"img-award\">\r\n                            </a>\r\n                            <figcaption class=\"caption-top p-2 rounded text-center bg-warning font-weight-bold\">");
#nullable restore
#line 62 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                                                                           Write(general.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</figcaption>\r\n                        </figure>\r\n                    </div>\r\n");
#nullable restore
#line 65 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                }
            }
        

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n    <div class=\"row mb-5 text-center\">\r\n");
#nullable restore
#line 70 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
          
            List<Awards> generals = ViewBag.General;
            if (generals.Count > 0)
            {
                foreach (var general in generals)
                {
                    String modalName = general.Id.ToString();
                    String imageurlgeneral = PamaWeb.GeneralClass.GeneralClass.backendUrl + "" + general.ImageUrl;

#line default
#line hidden
#nullable disable
            WriteLiteral("                    <div class=\"bod col-md-4\">\r\n                        <figure>\r\n                            <a href=\"#\" class=\"d-block mb-4\" data-toggle=\"modal\" data-target=\"#modaldirektur-");
#nullable restore
#line 80 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                                                                        Write(modalName);

#line default
#line hidden
#nullable disable
            WriteLiteral("\">\r\n                                <img class=\"w-100 imgsideaward img-fluid img-thumbnail\"");
            BeginWriteAttribute("src", " src=\"", 3855, "\"", 3877, 1);
#nullable restore
#line 81 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
WriteAttributeValue("", 3861, imageurlgeneral, 3861, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(" alt=\"img_award\">\r\n                            </a>\r\n                            <figcaption class=\"caption-general p-2 rounded text-center font-weight-bold\">");
#nullable restore
#line 83 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                                                                    Write(general.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral("</figcaption>\r\n                        </figure>\r\n                    </div>\r\n                    <div class=\"modal fade\"");
            BeginWriteAttribute("id", " id=\"", 4171, "\"", 4200, 2);
            WriteAttributeValue("", 4176, "modaldirektur-", 4176, 14, true);
#nullable restore
#line 86 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
WriteAttributeValue("", 4190, modalName, 4190, 10, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(@" tabindex=""-1"" role=""dialog"" aria-labelledby=""exampleModalLongTitle"" aria-hidden=""true"">
                        <div class=""modal-dialog"" role=""document"">
                            <div class=""modal-content"">
                                <div class=""modal-header bg-warning"">
                                    <h5 class=""modal-title"" id=""exampleModalLongTitle"">");
#nullable restore
#line 90 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                                                                                  Write(general.Title);

#line default
#line hidden
#nullable disable
            WriteLiteral(@"</h5>
                                    <button type=""button"" class=""close"" data-dismiss=""modal"" aria-label=""Close"">
                                        <span aria-hidden=""true"">&times;</span>
                                    </button>
                                </div>
                                <div class=""modal-body"">
                                    <img class=""img-fluid""");
            BeginWriteAttribute("src", " src=\"", 4993, "\"", 5015, 1);
#nullable restore
#line 96 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
WriteAttributeValue("", 4999, imageurlgeneral, 4999, 16, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            BeginWriteAttribute("alt", " alt=\"", 5016, "\"", 5036, 1);
#nullable restore
#line 96 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
WriteAttributeValue("", 5022, general.Title, 5022, 14, false);

#line default
#line hidden
#nullable disable
            EndWriteAttribute();
            WriteLiteral(">\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n");
#nullable restore
#line 101 "C:\Project\PamaFrontend\pama-web-core-migrate\pama-web-core\Views\Award\Index.cshtml"
                }
            }
        

#line default
#line hidden
#nullable disable
            WriteLiteral("    </div>\r\n</div>");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public IViewLocalizer Localizer { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
