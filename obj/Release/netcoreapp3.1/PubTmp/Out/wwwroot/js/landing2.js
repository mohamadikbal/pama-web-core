﻿window.addEventListener("scroll", myFunction);

function myFunction() {
    if (document.body.scrollLeft > 0 && document.body.scrollLeft < 1300 || document.documentElement.scrollLeft > 0 && document.documentElement.scrollLeft < 1300) {
        document.getElementById("forest").play();
        document.getElementById("excavator").pause();
    } else if (document.body.scrollLeft > 1300 || document.documentElement.scrollLeft > 1300) {
        document.getElementById("excavator").play();
        document.getElementById("forest").pause();
    } else {
        document.getElementById("excavator").pause();
        document.getElementById("forest").pause();
    }
}