﻿window.addEventListener("scroll", myFunction);

function myFunction() {
    if (document.body.scrollLeft > 1150 && document.body.scrollLeft < 3850 || document.documentElement.scrollLeft > 1150 && document.documentElement.scrollLeft < 3850) {
        document.getElementById("player").play();
        document.getElementById("typing").pause();
    } else if (document.body.scrollLeft > 3851 || document.documentElement.scrollLeft > 3851) {
        document.getElementById("typing").play();
        document.getElementById("player").pause();
    } else {
        document.getElementById("player").pause();
        document.getElementById("typing").pause();
    }
}