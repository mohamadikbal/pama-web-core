﻿window.addEventListener("scroll", myFunction);
function myFunction() {
    if (document.body.scrollLeft > 0 && document.body.scrollLeft < 900 || document.documentElement.scrollLeft > 0 && document.documentElement.scrollLeft < 900) {
        document.getElementById("player").play();
        document.getElementById("engine").pause();
    } else if (document.body.scrollLeft > 900 || document.documentElement.scrollLeft > 900) {
        document.getElementById("engine").play();
        document.getElementById("player").pause();
    } else {
        document.getElementById("engine").pause();
        document.getElementById("player").pause();
    }
}