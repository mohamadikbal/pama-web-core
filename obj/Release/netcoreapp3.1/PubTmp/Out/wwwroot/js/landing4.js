﻿window.addEventListener("scroll", myFunction);

function myFunction() {
    if (document.body.scrollLeft > 0 && document.body.scrollLeft < 1000 || document.documentElement.scrollLeft > 0 && document.documentElement.scrollLeft < 1000) {
        document.getElementById("talking").play();
    } else if (document.body.scrollLeft > 1200 || document.documentElement.scrollLeft > 1200) {
        document.getElementById("player").play();
        document.getElementById("talking").pause();
    } else {
        document.getElementById("player").pause();
        document.getElementById("talking").pause();
    }
}