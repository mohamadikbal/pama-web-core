﻿//Activate Tooltip
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

//Google maps API option
var map;
var InforObj = [];
var centerCords = {
    lat: -1.121881, lng: 114.481385
};
var markersOnMap = [{
    placeName: "PT. PAMAPERSADA NUSANTARA",
    LatLng: [{
        lat: -6.1975263, lng: 106.9154069
    }],
    placeDetail: "Head Office",
    description: "Jakarta Industrial Estate Pulo Gadung, Jl. Rawagelam I No.9, RW.9, Jatinegara, Kec. Cakung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta",
    imagedepan: "/../images/jobsite/head_office/hodalam.png",
    imagesamping: "/../images/jobsite/head_office/hosamping.png",
    imageatas: "/../images/jobsite/head_office/hoatas.png",
},
{
    placeName: "PAMA ABKL",
    LatLng: [{
        lat: -0.655074, lng: 117.069917
    }],
    placeDetail: "Office PAMA ABKL",
    description: "Batuah, Kec. Loa Janan, Kabupaten Kutai Kartanegara, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_abkl/abklmasjid.png",
    imagesamping: "/../images/jobsite/pama_abkl/abklmess.png",
    imageatas: "/../images/jobsite/pama_abkl/abklworkshop.png",
},
{
    placeName: "PAMA ADRO",
    LatLng: [{
        lat: -2.241437, lng: 115.483627
    }],
    placeDetail: "Office PAMA ADRO",
    description: "Dekat Padang Panjang, Tanta, Kabupaten Tabalong, Kalimantan Selatan",
    imagedepan: "/../images/jobsite/pama_adro/adrodepansmall.jpg",
    imagesamping: "/../images/jobsite/pama_adro/adrosamping.jpg",
    imageatas: "/../images/jobsite/pama_adro/adroatas.jpg",
},
{
    placeName: "PAMA ARIA",
    LatLng: [{
        lat: -3.837552, lng: 115.316015
    }],
    placeDetail: "Office PAMA ARIA",
    description: "Dekat Sungai Cuka, Kintap, Kabupaten Tanah Laut, Kalimantan Selatan",
    imagedepan: "/../images/jobsite/pama_aria/ariaoffice.png",
    imagesamping: "/../images/jobsite/pama_aria/ariamess.png",
    imageatas: "/../images/jobsite/pama_aria/ariaworkshop.png",
},
{
    placeName: "PAMA ASMI",
    LatLng: [{
        lat: -0.981275, lng: 114.380836
    }],
    placeDetail: "Office PAMA ASMI",
    description: "Dekat Barunang, Kec. Kapuas Tengah, Kabupaten Kapuas, Kalimantan Tengah",
    imagedepan: "/../images/jobsite/pama_asmi/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_asmi/officesamping.jpg",
    imageatas: "/../images/jobsite/pama_asmi/mess.jpg",
},
{
    placeName: "PAMA BAYA",
    LatLng: [{
        lat: -0.217742, lng: 117.102210
    }],
    placeDetail: "Office PAMA BAYA",
    description: "Buana Jaya, Tenggarong Seberang, Kutai Kartanegara Regency, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_baya/bayaoffice.png",
    imagesamping: "/../images/jobsite/pama_baya/bayamess.png",
    imageatas: "/../images/jobsite/pama_baya/bayaworkshop.png",
},
{
    placeName: "PAMA BBSO",
    LatLng: [{
        lat: -3.4490691, lng: 114.795016
    }],
    placeDetail: "Office PAMA BBSO",
    description: "Jalan A. Yani, Landasan Ulin, Guntungmanggis, Kec. Landasan Ulin, Kota Banjar Baru, Kalimantan Selatan",
    imagedepan: "/../images/jobsite/pama_bbso/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_bbso/officesamping.jpg",
    imageatas: "/../images/jobsite/pama_bbso/officeatas.jpg",
},
{
    placeName: "PAMA BEKB",
    LatLng: [{
        lat: -0.844015, lng: 115.493017
    }],
    placeDetail: "Office PAMA BEKB",
    description: "Dekat Besiq, Damai, West Kutai Regency, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_bekb/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_bekb/officeatas.jpg",
    imageatas: "/../images/jobsite/pama_bekb/mess.jpg",
},
{
    placeName: "PAMA BRCB",
    LatLng: [{
        lat: 1.914402, lng: 117.290135
    }],
    placeDetail: "Office PAMA Berau",
    description: "Dekat Bena Baru, Sambaliung, Berau Regency, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_brcb/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_brcb/officesamping.jpg",
    imageatas: "/../images/jobsite/pama_brcb/officeatas.jpg",
},
{
    placeName: "PAMA BPOP",
    LatLng: [{
        lat: -1.2239195, lng: 116.9625762
    }],
    placeDetail: "PT. Pamapersada Distrik BPOP",
    description: "Jl. Mulawarman, RT. 39, Kecamatan Balikpapan Timur, Manggar Baru, Kec. Balikpapan Tim., Kota Balikpapan, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_bpop/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_bpop/workshop.jpg",
    imageatas: "/../images/jobsite/pama_bpop/mess.jpg",
},
{
    placeName: "PAMA INDO",
    LatLng: [{
        lat: 0.149401, lng: 117.287658
    }],
    placeDetail: "Office PAMA INDO",
    description: "Dekat Suka Damai, Tlk. Pandan, Kabupaten Kutai Timur, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_indo/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_indo/messatas.jpg",
    imageatas: "/../images/jobsite/pama_indo/messsamping.jpg",
},
{
    placeName: "PAMA KIDE",
    LatLng: [{
        lat: -1.9286717, lng: 115.8593109
    }],
    placeDetail: "Office PAMA KIDECO",
    description: "Unnamed Rd, Rantau Bintungan, Muara Samu, Kabupaten Paser, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_kide/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_kide/officeatas.jpg",
    imageatas: "/../images/jobsite/pama_kide/mess.jpg",
},
{
    placeName: "PAMA MTBU",
    LatLng: [{
        lat: -3.747502, lng: 103.7486906
    }],
    placeDetail: "Office PT. Pamapersada Nusantara",
    description: "Tim., Sirah Pulau, Merapi Tim., Kabupaten Lahat, Sumatera Selatan",
    imagedepan: "/../images/jobsite/pama_mtbu/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_mtbu/officesamping.jpg",
    imageatas: "/../images/jobsite/pama_mtbu/officeatas.jpg",
},
{
    placeName: "PAMA KPCB",
    LatLng: [{
        lat: 0.801480, lng: 117.525787
    }],
    placeDetail: "Office PAMA KPCB",
    description: "Dekat Keraitan, Kec. Bengalon, Kabupaten Kutai Timur, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_kpcb/officeatas.jpg",
    imagesamping: "/../images/jobsite/pama_kpcb/officesamping.jpg",
    imageatas: "/../images/jobsite/pama_kpcb/mess.jpg",
},
{
    placeName: "PAMA TCMM",
    LatLng: [{
        lat: -0.716450, lng: 115.700996
    }],
    placeDetail: "Office PAMA TCMM",
    description: "Dekat Dilang Puti, Bentian Besar, Kabupaten Kutai Barat, Kalimantan Timur",
    imagedepan: "/../images/jobsite/pama_tcmm/tcmmoffice.png",
    imagesamping: "/../images/jobsite/pama_tcmm/tcmmmess.png",
    imageatas: "/../images/jobsite/pama_tcmm/tcmmworkshop.png",
},
{
    placeName: "PAMA TOPB",
    LatLng: [{
        lat: -1.121881, lng: 114.4813853
    }],
    placeDetail: "Office PAMA TOPB BUHUT",
    description: "Buhut Jaya, Kec. Kapuas Tengah, Kabupaten Kapuas, Kalimantan Tengah",
    imagedepan: "/../images/jobsite/pama_topb/topboffice.png",
    imagesamping: "/../images/jobsite/pama_topb/topbmess.png",
    imageatas: "/../images/jobsite/pama_topb/topbport.png",
},
{
    placeName: "PAMA KPCS",
    LatLng: [{
        lat: 0.576462, lng: 117.465714
    }],
    placeDetail: "PT Pamapersada Distrik KPCS",
    description: "Bintang Area, KPC Mine Project, Kalimantan Tengah",
    imagedepan: "/../images/jobsite/pama_kpcs/officedepan.jpg",
    imagesamping: "/../images/jobsite/pama_kpcs/officesamping.jpg",
    imageatas: "/../images/jobsite/pama_kpcs/officeatas.jpg",
},
{
    placeName: "PAMA SMMS",
    LatLng: [{
        lat: -1.0556523, lng: 114.6801648
    }],
    placeDetail: "Office PT PAMA SMMS Sekako",
    description: "Lemo I, Kec. Teweh Tengah, Kabupaten Barito Utara, Kalimantan Tengah",
    imagedepan: "/../images/jobsite/pama_smms/messatas.jpg",
    imagesamping: "/../images/jobsite/pama_smms/officesamping.jpg",
    imageatas: "/../images/jobsite/pama_smms/officeatas.jpg",
},
];

window.onload = function () {
    initMap();
};

function addMarkerInfo() {
    for (var i = 0; i < markersOnMap.length; i++) {
        var contentString =
            '<div id="content">' +
            '<h1>' + markersOnMap[i].placeName +
            '</h1><h5>' + markersOnMap[i].placeDetail + '</h5>' +
            '<p>' + markersOnMap[i].description + '</p>' +
            '</div>' +
            '<div id="imgjobsite">' +
            '<img style="padding: 3px; width: 200px; height: 150px; object-fit: cover;" src="' + markersOnMap[i].imagedepan + '" alt="tampakdepan">' +
            '<img style="padding: 3px; width: 200px; height: 150px; object-fit: cover;" src="' + markersOnMap[i].imagesamping + '" alt="tampaksamping">' +
            '<img style="padding: 3px; width: 200px; height: 150px; object-fit: cover;" src="' + markersOnMap[i].imageatas + '" alt="tampakatas">' +
            '</div>';

        const marker = new google.maps.Marker({
            position: markersOnMap[i].LatLng[0],
            map: map
        });

        const infowindow = new google.maps.InfoWindow({
            content: contentString,
            maxWidth: 700
        });

        marker.addListener('click', function () {
            closeOtherInfo();
            infowindow.open(marker.get('map'), marker);
            InforObj[0] = infowindow;
        });
        // marker.addListener('mouseover', function () {
        //     closeOtherInfo();
        //     infowindow.open(marker.get('map'), marker);
        //     InforObj[0] = infowindow;
        // });
        // marker.addListener('mouseout', function () {
        //     closeOtherInfo();
        //     infowindow.close();
        //     InforObj[0] = infowindow;
        // });
    }
}

function closeOtherInfo() {
    if (InforObj.length > 0) {
        /* detach the info-window from the marker ... undocumented in the API docs */
        InforObj[0].set("marker", null);
        /* and close it */
        InforObj[0].close();
        /* blank the array */
        InforObj.length = 0;
    }
}

function initMap() {
    map = new google.maps.Map(document.getElementById('map'), {
        zoom: 6,
        center: centerCords
    });
    addMarkerInfo();
}