﻿window.addEventListener("scroll", myFunction);

function myFunction() {
    if (document.body.scrollLeft > 0 && document.body.scrollLeft < 600 || document.documentElement.scrollLeft > 0 && document.documentElement.scrollLeft < 600) {
        document.getElementById("airplane").play();
    } else if (document.body.scrollLeft > 700 && document.body.scrollLeft < 1700 || document.documentElement.scrollLeft > 700 && document.documentElement.scrollLeft < 1700) {
        document.getElementById("player").play();
        document.getElementById("lovesong").pause();
        document.getElementById("airplane").pause();
    } else if (document.body.scrollLeft > 1700 || document.documentElement.scrollLeft > 1700) {
        document.getElementById("lovesong").play();
        document.getElementById("player").pause();
        document.getElementById("airplane").pause();
    } else {
        document.getElementById("player").pause();
        document.getElementById("airplane").pause();
        document.getElementById("lovesong").pause();
    }
}