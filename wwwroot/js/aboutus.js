﻿//Function Navigation
function openNav() {
    document.getElementById("open").style.display = "none";
    document.getElementById("nav").style.width = "100%";
}

function closeNav() {
    document.getElementById("nav").style.width = "0%";
    document.getElementById("open").style.display = "block";
}

//Fullpage
new fullpage('#fullpage', {
    //options for fullpage here
    licenseKey: 'F71F4C58-42C146D1-A4FA6975-543EA442',
    scrollOverflow: true,
    autoScrolling: true,
    anchors: ['SectionHome', 'SectionMainGlance', 'SectionPortfolio', 'SectionContactUs'],
    scrollingSpeed: 1100,
    verticalCentered: true,
});
