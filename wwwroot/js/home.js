﻿//Function Navigation
function openNav() {
    document.getElementById("open").style.display = "none";
    document.getElementById("nav").style.width = "100%";
}

function closeNav() {
    document.getElementById("nav").style.width = "0%";
    document.getElementById("open").style.display = "block";
}

//Fullpage
new fullpage('#fullpage', {
    //options for fullpage here
    licenseKey: 'F71F4C58-42C146D1-A4FA6975-543EA442',
    scrollOverflow: true,
    autoScrolling: true,
    navigation: true,
    anchors: ['SectionHome', 'SectionEducation', 'SectionJobsite', 'SectionHistory', 'SectionNews', 'SectionManPower', 'SectionContactUs'],
    navigationTooltips: ['Home', 'E-Learning', 'Jobsite', 'History', 'Media', 'Man Power', 'Contact Us'],
    scrollingSpeed: 1100,
    slidesNavigation: true,
    slidesNavPosition: 'center',
    controlArrows: false,
    normalScrollElements: '.modal, .history-wrapper',
    verticalCentered: false,
});

//Jquery Modal
$('#myModal').modal('show');
